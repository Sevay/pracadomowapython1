def check_name(name: str) -> None:
    if name == "Maciej":
        print("Halo halo Maciej")
    else:
        print("Witaj {}".format(name))


check_name("Maciej")
check_name("Jasiu")


def check_number(number: int) -> None:
    if number:
        print(number)
    else:
        print("Dowolony napis")


check_number(-5)
check_number(10)
check_number(0)


class Block:
    def __init__(self, name: str, volume: int):
        self.name = name
        self.volume = volume

    def __eq__(self, other):
        return self.volume == other.volume

    def __lt__(self, other):
        return self.volume < other.volume

    def __gt__(self, other):
        return self.volume > other.volume

    def __le__(self, other):
        return self.volume <= other.volume

    def __ge__(self, other):
        return self.volume >= other.volume

    def __ne__(self, other):
        return self.volume != other.volume


brick = Block("Prostopadłościan", 52)
cube = Block("Sześcian", 40)
other_block = Block("Bryła", 52)

print(brick == cube, brick < cube, brick > cube, brick <= cube, brick >= cube, brick != cube)
print(brick == other_block, brick < other_block, brick > other_block, brick <= other_block, brick >= other_block,
      brick != other_block)
print(cube == other_block, cube < other_block, cube > other_block, cube <= other_block, cube >= other_block,
      cube != other_block)


class Figure:
    def __init__(self, name: str, area: int):
        self.name = name
        self.area = area

    def __eq__(self, other):
        return self.area == other.area

    def __lt__(self, other):
        return self.area < other.area

    def __gt__(self, other):
        return self.area > other.area

    def __le__(self, other):
        return self.area <= other.area

    def __ge__(self, other):
        return self.area >= other.area

    def __ne__(self, other):
        return self.area != other.area


square = Figure("kwadrat", 25)
rect = Figure("Prostokąt", 30)
print(square == rect, square < rect, square > rect, square <= rect, square >= rect, square != rect)


